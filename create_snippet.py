#!/usr/bin/env python3

import requests
import sys
import os.path
import re
import shutil
from git import Repo
from tempfile import TemporaryDirectory
from urllib.parse import quote, unquote

# 
# Create a snippet with a name and the content of a file (named README.md)
# Returns the snippet (from the Gitlab API response)
# 
def create_snippet(gitlab_token, name, content):
    payload = {
            "title": name,
            "visibility": "public",
            "files": [
                {
                    "content": content,
                    "file_path": "README.md"
                }
            ]
        }
    r = requests.post(
        "https://gitlab.com/api/v4/snippets",
        headers= {
            'PRIVATE-TOKEN': gitlab_token,
        },
        json = payload,
    )
    if r.status_code != 201:
        print(f'Err with Gitlab API: {r.status_code} {r.reason}')
        exit(-1)
    response = r.json()
    return response

#
# Retrieve a snippet from its identifier
# Returns the snippet (from the Gitlab API response)
# 
def get_snippet(gitlab_token, id):
    r = requests.get(f"https://gitlab.com/api/v4/snippets/{id}",
        headers= {
            'PRIVATE-TOKEN': gitlab_token,
        })
    if r.status_code != 200:
        print(f'Err with Gitlab API: {r.status_code} {r.reason}')
        exit(-1)
    response = r.json()
    return response


#
# Retrieve a file of a snippet from the snippet identifier and the file name
# Returns the raw content of the file
# 
def get_snippet_file(gitlab_token, id, filename):
    r = requests.get(f"https://gitlab.com/api/v4/snippets/{id}/files/main/{filename}/raw",
        headers= {
            'PRIVATE-TOKEN': gitlab_token,
        })
    if r.status_code != 200:
        print(f'Err with Gitlab API: {r.status_code} {r.reason}')
        exit(-1)
    return r.text

# 
# Update the existing README.md of a snippet with a new content
# Returns the updated snippet (from the Gitlab API response)
#
def update_snippet(gitlab_token, snippet, new_content):
    payload = {
        "files": [ {
                'action': 'update', 
                'file_path': 'README.md',
                'content': new_content,
            } ]
    }
    r = requests.put(
        f'https://gitlab.com/api/v4/snippets/{snippet["id"]}',
        headers= {
            'PRIVATE-TOKEN': gitlab_token,
        },
        json = payload,
    )
    if r.status_code != 200:
        print(f'Err with Gitlab API: {r.status_code} {r.reason}')
        exit(-1)
    response = r.json()
    return response


# 
# List the files referenced in markdown (ie. any URL not starting with "http")
# Returns a list of filenames
# 
def list_files_in_markdown(markdown):
    files = re.findall(r'\[.*?\]\((.*?)\)', markdown)
    files = filter(lambda x: not x.startswith('http'), files)
    files = [ unquote(x) for x in files ]
    return set(files)

#
# Rewrite a markdown by replacing a list of links to filenames by
# absolute URLs provided as argument.
# Returns the updated markdown content
#
def rewrite_markdown_urls(markdown, files, url):
    for f in files:
        quoted_f = quote(f)
        rewritten_f = f'{url}/raw/main/{quoted_f}'
        print(f'\t{quoted_f} => {rewritten_f}')
        markdown = markdown.replace(f'({quoted_f})', f'({rewritten_f})')
    return markdown


#
# Add files to a snippet, byt cloning its git repository and adding them.
# Takes as argument a snippet object, a directory containing the files, 
# and a list of filenames.
#
def add_files_to_snippet(snippet, directory, files_to_add):
    with TemporaryDirectory() as workdir:
        print(f'\tCloning snippet git repo in {workdir}')
        repo = Repo.clone_from(snippet['ssh_url_to_repo'], workdir)
        for f in files_to_add:
            print(f'\tAdding file {f}')
            shutil.copyfile(f'{directory}/{f}', f'{workdir}/{f}')
            repo.git.add(f)
        print(f'\tCommit & push')
        repo.git.commit(message='Adding attached files')
        repo.git.push()


#
# Check if a list of files are readable in directory base_dir
# Return the list of files that are not
#
def check_files_access(base_dir, list_files):
    result = list()
    for file in list_files:
        fn = os.path.join(base_dir, file)
        if not os.path.exists(fn):
            result.append(file)
    return result


if __name__ == '__main__':
    #
    # Checking if the Gitlab token is provided
    #
    if not os.path.exists('.token'):
        print('You need to provide a Gitlab API token in the file .token')
        exit(-1)
    gitlab_token = open('.token', 'r').readline()

    # 
    # Load & check arguments
    # 
    if len(sys.argv) != 3:
        print(f'Syntax: {sys.argv[0]} <name> <markdown_file>')
        exit(-1)
    _, name, markdown_file = sys.argv
    base_project = os.path.dirname(markdown_file)

    if not os.path.exists(markdown_file):
        print(f'Unable to open {markdown_file}')
        exit(-1)
    markdown = open(markdown_file, 'r').read()

    #
    # The main program
    #
    print(f'Create snippet from {markdown_file}')
    snippet = create_snippet(gitlab_token, name, markdown)

    print(f'Snippet has been created: {snippet["web_url"]}')
    input('You can edit it to your purpose. Press enter when ready to publish.')
    snippet = get_snippet(gitlab_token, snippet['id'])
    markdown = get_snippet_file(gitlab_token, snippet['id'], 'README.md')

    print(f'Detect & rewrite attached files...')
    embedded_files = list_files_in_markdown(markdown)
    if len(embedded_files) > 0:
        troubles = check_files_access(base_project, embedded_files)
        if len(troubles) != 0:
            print(f'Some embedded files are not readable: ', ', '.join(troubles))
            answer = input('Do you want to continue (y/N) ?')
            if answer.lower() != 'y':
                exit(-1)

        markdown = rewrite_markdown_urls(markdown, embedded_files, snippet["web_url"])
        snippet = update_snippet(gitlab_token, snippet, markdown)
        print(f'Add attached files to the snippet')
        add_files_to_snippet(snippet, base_project, embedded_files)
    print(f'Snippet created : {snippet["web_url"]}')
