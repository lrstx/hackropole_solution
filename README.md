# hackropole_solution

## Objectif

Ce projet est un script python permettant l'automatisation de la création
de snippets Gitlab pour soumettre des solutions sur la plateforme 
[hackropole.fr](https://hackropole.fr/).

**Disclaimer : ce n'est pas un projet officiel d'hackropole.fr, je ne fais pas partie de l'organisation.** Il s'agit simplement d'une 
automatisation venant d'un joueur (moi).

Le besoin vient du fait que mes write-ups sont écrits sous forme de README dans
le repository [ctf-writeups](https://gitlab.com/ctfun/ctf-writeups), mais les
organisateurs demandent des snippets qui contiennent toutes les pièces jointes,
pour des raisons de sécurité.

# Pré-requis

- Installer les dépendances listées dans `requirements.txt`.
- Fournir un [Access Token Gitlab](https://gitlab.com/-/user_settings/personal_access_tokens) avec le scope `api` dans un fichier `.token`.
- Avoir configuré git avec [des clefs SSH pour Gitlab](https://gitlab.com/-/user_settings/ssh_keys).

# Usage

Le script demande un nom et un fichier markdown qui contient la solution. Exemple de fonctionnement :
```shell
$ ./create_snippet.py "FCSC2024 / Hardware / Castafiore" <REDACTED>/fcsc2024/hardware/Castafiore/README.md 
Create snippet from <REDACTED>/fcsc2024/hardware/Castafiore/README.md
=> https://gitlab.com/-/snippets/3704652 / git@gitlab.com:snippets/3704652.git
Detect & rewrite attached files...
	audacity.png => https://gitlab.com/-/snippets/3704652/raw/main/audacity.png
	Castafiore.wav => https://gitlab.com/-/snippets/3704652/raw/main/Castafiore.wav
Update snippet with rewritten markdown
Add attached files to the snippet
	Cloning snippet git repo in /tmp/tmpuaeexxgb
	Adding file audacity.png
	Adding file Castafiore.wav
	Commit & push
Snippet created => https://gitlab.com/-/snippets/3704652
```


## Fonctionnement

Le script présent ici automatise la génération d'un snippet à partir d'un write-up :
- crée un snippet à partir d'un fichier Markdown.
- liste les pièces jointes et réécrit leurs URLs avec celle du snippet.
- pousse la nouvelle version du markdown dans le snippet.
- ajoute les pièces jointes via le repo git (car impossible d'ajouter des fichiers binaires par l'API).

Attention, dès lors que des fichiers binaires ont été ajouté, l'édition du
snippet n'est plus possible depuis la GUI ([bug](https://gitlab.com/gitlab-org/gitlab/-/issues/350104) ou feature ?).